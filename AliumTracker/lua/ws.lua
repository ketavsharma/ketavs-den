local cjson = require "cjson"
local cjson2 = cjson.new()
local cjson_safe = require "cjson.safe"
local server = require "resty.websocket.server"
local redis = require "resty.redis"
id = 1;
local red = redis:new()
red:set_timeout(1000)
local ok, err = red:connect("127.0.0.1", 6379)
if not ok then
                    ngx.say("failed to connect: ", err)
                    return
end
local request_host = ngx.var.host
local ua = ngx.escape_uri(ngx.req.get_headers()["User-Agent"])
local ip =ngx.escape_uri(ngx.var.remote_addr);
    local wb, err = server:new{
      timeout = 5000,
      max_payload_len = 65535
    }
    if not wb then
      ngx.log(ngx.ERR, "failed to new websocket: ", err)
      return ngx.exit(444)
    end
    while true do
      local data, typ, err = wb:recv_frame()
      if wb.fatal then
        ngx.log(ngx.ERR, "failed to receive frame: ", err)
        return ngx.exit(444)
      end
      if not data then
        local bytes, err = wb:send_ping()
        if not bytes then
          ngx.log(ngx.ERR, "failed to send ping: ", err)
          return ngx.exit(444)
        end
      elseif typ == "close" then break
      elseif typ == "ping" then
        local bytes, err = wb:send_pong()
        if not bytes then
          ngx.log(ngx.ERR, "failed to send pong: ", err)
          return ngx.exit(444)
        end
      elseif typ == "pong" then
        ngx.log(ngx.INFO, "client ponged")
      elseif typ == "text" then
        uidata = cjson.decode(data)
        keyredis = uidata['uid']
        uidata['ngnixts'] = ngx.time()
        data = cjson.encode(uidata)
        finalData = data..'nextjson'
        ok,err = red:append(keyredis,finalData)
        if not ok then
                ngx.say("failed to set key",err)
                return
        end
        ok,err = red:expire(keyredis,10800)
        if not ok then
                ngx.say("failed to set expiry",err)
                return
        end
        id=id+1
      end
    end
    wb:send_close()
