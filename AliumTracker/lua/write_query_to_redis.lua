local redis = require "resty.redis"
local red = redis:new()
red:set_timeout(1000)
local ok, err = red:connect("127.0.0.1", 6379)
if not ok then
                    ngx.say("failed to connect: ", err)
                    return
end
local res, err = red:sadd("data"..os.date("%Y-%m-%d-%H-%M",os.time()-24*60*60),ngx.var.QUERY_STRING.."&ngxtime="..ngx.time())
    if not res then
        ngx.say("2: failed to publish: ", err)
        return
    end