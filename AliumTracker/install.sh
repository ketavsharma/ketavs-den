sudo apt-get update
wget -qO - https://openresty.org/package/pubkey.gpg | sudo apt-key add -
sudo apt-get -y install software-properties-common
sudo add-apt-repository -y "deb http://openresty.org/package/ubuntu $(lsb_release -sc) main"
sudo apt-get update -y 
sudo apt-get install -y openresty redis-server mariadb-server mariadb-client python-pip python-dev libmysqlclient-dev
pip install --upgrade pip
pip install mysqlclient redis schedule
mkdir /usr/local/openresty/luascripts
mv lua/ws.lua /usr/local/openresty/luascripts/
mysql -uroot -e "CREATE database aliumsurveys;use aliumsurveys;CREATE TABLE SurveyTracker (clientid VARCHAR(50), data VARCHAR(5000),userid VARCHAR(100), visitnum VARCHAR(100));"
mkdir $HOME/pythonapp
mv python/redisparser.py $HOME/pythonapp/
mv nginx/nginx_1.conf /etc/openresty/nginx.conf
service openresty restart